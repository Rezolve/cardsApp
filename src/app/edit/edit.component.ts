import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../services/profile.service';
import { Profile } from '../profile';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { MessageService } from '../services/message.service';

declare var $: any;

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
	public file_src:string = '../assets/images/avatar3.png';
	profile: Profile;
	fullname: string='';

  constructor(
  	private _profileService: ProfileService,
  	private route: ActivatedRoute,
  	private router: Router,
  	private messageService: MessageService
  	) { }

  ngOnInit() {
  	this.getOneProfile();
  }

  getOneProfile() {
  	var id = this.route.snapshot.params['id'];

  	this._profileService
  		.showProfile(id)
  		.subscribe(profile => {
  			this.profile = profile[0];
  			this.profile.id = id;

  			//console.log(this.profile);
  			this.file_src = this.profile.photo;
  		});
  }

  imageUploaded(file: any) {
  	$('img').hide();
  	//$('btn-save').removeAttr('disabled');
  	this.profile.photo = file.src;
  }

  imageRemoved(file: any){
  	$('img').show();
  	$('#btn-save').attr('disabled','disabled');
  	this.profile.photo = '';
  	this.file_src = '../assets/images/avatar3.png';
  }

  goBack() {
  	this.router.navigate(['/home']);
  }

  updateProfile() {
  	this._profileService
  		.editProfile(this.profile)
  		.subscribe(response => {
  			console.log(response);
  			this.messageService.showMessage("div#msg", "alert-info", "Profile has been successfully Updated.", "glyphicon-ok");
  		})
  }

}
