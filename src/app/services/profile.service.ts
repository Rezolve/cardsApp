import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class ProfileService {

	//local Laravel server
	server = 'http://profile/';

	headers: Headers = new Headers;
	options : any;

  constructor(private _http:Http) {
  	this.headers.append('enctype', 'multipart/form-data');
  	this.headers.append('Content-Type', 'application/json');
  	this.headers.append('X-Requested-With', 'XMLHttpRequest');
  	this.options = new RequestOptions({ headers: this.headers });
  }

  addProfile(info) {
  	var data = JSON.stringify(info);

  	return this._http.post(this.server+"addprofile",data,this.options).map(res => res.json());
  }

  showAllProfiles() {
    return this._http.get(this.server+"profiles").map(res => res.json());
  }

  showProfile(id) {
    return this._http.get(this.server+"select_profile/"+id).map(res => res.json());
  }

  editProfile(info) {
    var data = JSON.stringify(info);
    return this._http.post(this.server+"editprofile",data,this.options).map(res => res.json());
  }

  deleteProfile(id) {
    return this._http.get(this.server+"delete_profile/"+id).map(res => res.json());
  }
}