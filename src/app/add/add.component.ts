import { Component, OnInit } from '@angular/core';
import { Profile } from '../profile';

import { ActivatedRoute, Params, Router } from '@angular/router';

import { ProfileService } from '../services/profile.service';
import { MessageService } from '../services/message.service';

declare var $: any;

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

	public file_src: string = '../assets/images/avatar3.png';

  constructor(
    private router: Router,
    private profileService: ProfileService,
    private messageService: MessageService
    ) { }

  ngOnInit() {
  	}

    model = new Profile ();

    addProfile() {
      this.profileService
        .addProfile(this.model)
        .subscribe(response => {
          console.log(response);
          this.messageService.showMessage("div#msg1", "alert-info", "New Profile has been successfully Added.", "glyphicon-ok");
        })
    }

  	imageUploaded(file: any) {
  		$('img').hide();
      this.model.photo = file.src;
  	}

  	imageRemoved (file: any) {
  		$('img').show();
  	}

   	goBack() {
  		this.router.navigate(['/home']);
  	}

  }

