import { Component, OnInit } from '@angular/core';
import { Profile } from '../profile';

import { ActivatedRoute, Params, Router } from '@angular/router';

import { ProfileService } from '../services/profile.service';
import { MessageService } from '../services/message.service';

declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor( private _profileService: ProfileService,
  				private messageService: MessageService,
  				private router: Router ) { }

  profiles: any;

  ngOnInit() {
  	this.getAllProfiles();
  }

  getAllProfiles() {
  	this._profileService
  		.showAllProfiles()
  		.subscribe(profiles => {
  			this.profiles = profiles;
  			//console.log(this.profiles);
  		})
  }

  deleteProfile(id) {
  	var response = confirm ("Are you sure?");
  	if (response == true) {
  		this._profileService
  			.deleteProfile(id)
  			.subscribe(response => {
  				console.log(response);
  				this.getAllProfiles();
  				this.messageService.showMessage("div#msg", "alert-info", "Profile has been deleted.", "glyphicon-ok")
  			})
  	} else {
  		//cancel button
  	}
  }

}
